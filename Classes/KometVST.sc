KometVST : KometSynthLibExt{
    *initClass{
        Class.initClassTree(KometSynthLib);
        if((this.class != KometSynthLibExt) && (this.class != Meta_KometSynthLibExt), {
            "Detected Kometsynthlib extension %".format(this.name).postln;
        });

        StartUp.add({
            KometSynthLib.files[\synths] = KometSynthLib.files[\synths] ++ this.synths().files;
            KometSynthLib.files[\fx] = KometSynthLib.files[\fx] ++ this.fx().files;
            // KometSynthLib.files[\parfx] = KometSynthLib.files[\parfx] ++ this.parfx().folders.collect{|dir| dir.files}.flatten;
            // KometSynthLib.files[\faust] = KometSynthLib.files[\faust] ++ this.faust().folders.collect{|dir| dir.files}.flatten;
        })
    }
}
